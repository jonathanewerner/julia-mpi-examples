import MPI
macro p(s)
    :(print(string($s, "\n")))
end
macro pr(s)
    # :(print(string("[id $id @ $(gethostname())] ", $s, "\n")))
    :(print(string("[$rank] ", $s, "\n")))
end

macro root(expr)
    :(rank == root && $expr)
end




function main()
    MPI.init()
    # function recv(source::Integer, tag::Integer, c::Comm)
    # function send(A, dest::Integer, tag::Integer, c::Comm)
    # rreq = MPI.Irecv!(recv_mesg, src,  src+32, comm)

    comm = MPI.COMM_WORLD
    processes_count = MPI.size(comm)
    rank = MPI.rank(comm)
    root = 0
    @root tic()
    # send(what, to) = MPI.Isend!(what, to, 0, comm)
    # recv(into, from)= MPI.Irecv(into, from, 0, comm)


    size = 1_000_000_00
    # sreq = send(id,partner)
    if rank != root
        sreq = MPI.Isend!(ones(size), root, 0, comm)
    else
        a=Array(Float64, size)
        rreq = MPI.Irecv!(a, 1, 0, comm)
    end
    # MPI.waitall([sreq, rreq])


    # @pr partners_id
    


    MPI.finalize()
    @root toc()
end

main()

# MPI.Bcast!(A,length(A), root, comm)
# @pr A


# fill!(send_mesg, float64(rank))
# rreq = MPI.Irecv!(recv_mesg, src,  src+32, comm)
# sreq = MPI.Isend!(send_mesg, dst, rank+32, comm)

# stats = MPI.waitall!([rreq, sreq])

# MPI.barrier(comm)
