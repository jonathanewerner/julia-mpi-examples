import MPI
macro p(s)
    :(print(string($s, "\n")))
end
macro pr(s)
    # :(print(string("[id $id @ $(gethostname())] ", $s, "\n")))
    :(print(string("[$id] ", $s, "\n")))
end

macro root(expr)
    :(id == root && $expr)
end

macro nonroot(expr)
    :(id != root && $expr)
end

macro waitall()
    :(MPI.barrier(comm))
end
# function recv(source::Integer, tag::Integer, c::Comm)
# function send(A, dest::Integer, tag::Integer, c::Comm)
# rreq = MPI.Irecv!(recv_mesg, src,  src+32, comm)

MPI.init()
comm = MPI.COMM_WORLD
processes_count = MPI.size(comm)
id = MPI.rank(comm)
root = 0

send(what, to, tag) = MPI.send(what, to, tag, comm)
recv_from(from, tag) = MPI.recv(from, tag, comm)
# wait_for(requests...) = MPI.waitall(requests...)

function main()
    @root tic()
    @nonroot @pr "hello from not root"
    size = 1000

    elements_per_process = div(size, processes_count)
    old = zeros(elements_per_process+2)
    new = zeros(elements_per_process+2)
    @root old[3] = 1000

    next_id = (id == processes_count-1 ? 0 : id+1)
    prev_id = (id == 0 ? processes_count-1 : id-1)

    timesteps =5000
    for t=1:timesteps
        if t > 1
            old[1] = recv_from(prev_id, 1111)
            old[end] = recv_from(next_id, 8888)
            # @waitall
        end

        for i=2:elements_per_process+1
            new[i] = (old[i] + old[i+1] + old[i-1])/3;
        end

        if t < timesteps
            send(new[2], prev_id, 8888)
            send(new[end-1], next_id, 1111)
        end
        old, new = new, old
    end

    # output
    send(old[2:elements_per_process+1], root, 3333)

    if id == root
        result = old[2:elements_per_process+1]
        for x=1:processes_count-1
            arr = recv_from(x, 3333)
            result = vcat(result, arr)
        end
    end

    # @root @pr result

    @root toc()

    MPI.finalize()
end

main()

# MPI.Bcast!(A,length(A), root, comm)
# @pr A


# fill!(send_mesg, float64(rank))
# rreq = MPI.Irecv!(recv_mesg, src,  src+32, comm)
# sreq = MPI.Isend!(send_mesg, dst, rank+32, comm)

# stats = MPI.waitall!([rreq, sreq])

# MPI.barrier(comm)
